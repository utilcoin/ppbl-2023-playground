// Import Statements:
import {
	Box,
	Heading,
	FormControl,
	FormLabel,
	Input,
	Button,
	Text,
} from '@chakra-ui/react';
import { useFormik } from 'formik';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { KoiosProvider, UTxO } from '@meshsdk/core';

// The <KoiosComponentExample /> that can be imported to /ppbl-2023-front-end-starter/src/pages/index.tsx
export const KoiosComponentExample = () => {
	// useState hook to store address from user input
	const [queryAddress, setQueryAddress] = useState<string | undefined>(
		undefined
	);
	const koiosProvider = new KoiosProvider('preprod');
	const [koiosResultUTxOs, setKoiosResultUTxOs] = useState<UTxO[] | undefined>(
		undefined
	);

	// useFormik hook from formik library
	// Makes it easier to handle forms in React
	const formik = useFormik({
		initialValues: {
			cardanoAddress: '',
		},
		onSubmit: (values) => {
			setQueryAddress(values.cardanoAddress);
		},
	});

	// When a button is clicked, set the queryAddress
	const handleClick = () => {
		setQueryAddress(formik.values.cardanoAddress);
	};

	// When queryAddress is set, use koiosProvider.fetchAddressUTxOs to get get UTxOs at address
	useEffect(() => {
		const fetchAddressUTxOs = async () => {
			if (queryAddress) {
				const result = await koiosProvider.fetchAddressUTxOs(queryAddress);
				setKoiosResultUTxOs(result);
			}
		};
		if (queryAddress) {
			fetchAddressUTxOs();
		}
	}, [queryAddress]);

	// Rendered on the page:
	return (
		<Box bg='theme.light' color='theme.dark' p='3' mt='5'>
			<Heading size='md' py='3'>
				Example with KoiosProvider
			</Heading>
			<Text py='3'>
				This form returns a list of UTxOs for any Preprod Address.
			</Text>
			<FormControl bg='theme.dark' color='theme.light' p='5'>
				<FormLabel>Enter a Cardano Preprod Address:</FormLabel>
				<Input
					mb='3'
					id='cardanoAddress'
					name='cardanoAddress'
					onChange={formik.handleChange}
					value={formik.values.cardanoAddress}
					placeholder='Preprod Address'
				/>
				<Button onClick={handleClick} size='sm'>
					Check Address
				</Button>
			</FormControl>

			{koiosResultUTxOs && (
				<Box fontSize='xs'>
					<pre>{JSON.stringify(koiosResultUTxOs, null, 2)}</pre>
				</Box>
			)}
		</Box>
	);
};