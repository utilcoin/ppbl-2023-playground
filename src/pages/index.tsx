import Head from "next/head";
import { Box, Divider, Link as CLink, Heading, Text, UnorderedList, ListItem } from "@chakra-ui/react";
import { ContributorComponent } from "@/src/components/ContributorComponent";
import { QueryComponent } from "@/src/components/QueryComponent";
import { CardanoWallet } from "@meshsdk/react";
import Link from "next/link";

export default function Home() {
  return (
    <>
      <Head>
        <title>PPBL 2023 Playground</title>
        <meta name="description" content="Plutus Project-Based Learning from Gimbalabs" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Divider w="70%" mx="auto" pb="10" />
      <Box w={["100%", "70%"]} mx="auto" my="10">
        <Heading pb="5">PPBL 2023 Playground</Heading>
        <CLink href="/students/template">View /students/template</CLink>

        <Box mt="20" p="auto">
          <Heading mb="5">Take a look at what your peers have worked on</Heading>
          <Box h="5" />
          <CLink href="/students/contributor-token-details">By contributor-token-details</CLink>
          <Box h="5" />
          <CLink href="/students/wallet-hooks">By wallet-hooks</CLink>
          <Box h="5" />
          <CLink href="/students/byRahat">By Rahat [PPBL NFTs Collection]</CLink>
          <Box h="5" />
          <CLink href="/students/templateClaudio">By Claudio</CLink>
          <Box h="5" />
          <CLink href="/students/nelsonksh">By nelsonksh</CLink>
          <Box h="5" />
          <CLink href="/students/Faizan">By Faizan</CLink>
          <Box h="5" />
          <CLink href="/students/ash">By ash</CLink>
          <Box h="5" />
          <CLink href="/students/gulla0">By gulla0</CLink>
          <Box h="5" />
          <CLink href="/students/northZeta">By northZeta</CLink>
          <Box h="5" />
          <CLink href="/students/robertomh">By robertomh</CLink>
          <Box h="5" />
          <CLink href="/students/tamhv">By tamhv</CLink>
          <Box h="5" />
          <CLink href="/students/newman5">By newman5</CLink>
          <Box h="5" />
          <CLink href="/students/sebastianpabon">By Sebastian Pabon</CLink>


        </Box>


      </Box>

    </>
  );
}
