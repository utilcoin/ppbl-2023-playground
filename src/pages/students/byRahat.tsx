import Head from "next/head";
import React from "react";
import { Box, Divider, Link as CLink, Heading, Text } from "@chakra-ui/react";
import { CardanoWallet } from "@meshsdk/react";
import { useEffect, useState } from "react";
import { BlockfrostProvider } from "@meshsdk/core";

export default function byRahat() {
  const blockfrostProvider = new BlockfrostProvider(
    "preproduQWo54ldIPVQjCvVIDqKYJhDH8JAMy39"
  );
  // console.log('Rahat component rendered');
  const [data, setData] = useState([]);
  const [mdata, setMetadata] = useState("");
  const policyId = "2a384dc205a97463577fc98b704b537f680c0eba84126eb7d5857c86";

  useEffect(() => {
    async function findAssetsByPolicyId(policyId: string) {
      try {
        let cursor = 1;
        let allAssets: any[] | ((prevState: never[]) => never[]) = [];

        while (true) {
          const response = await blockfrostProvider.fetchCollectionAssets(
            policyId,
            cursor
          );
          const assets = response.assets;
          console.log("Assets with Policy ID:", policyId);
          // console.log(assets);

          if (Array.isArray(assets) && assets.length > 0) {
            allAssets = [...allAssets, ...assets];
          } else {
            break;
          }

          if (response.next === null) {
            break;
          }

          // Increment cursor for the next page
          cursor++;
        }
        setData(allAssets);
        console.log("useeffect", allAssets);
        // allAssets.forEach((item) => {
        //   fetchAssetMetadata(item.unit);
        // });
      } catch (error) {
        console.error("Error fetching assets:", error);
      }
    }

    // Call the function
    findAssetsByPolicyId(policyId);
    console.log("1st useEffect");
  }, []);


  useEffect(() => {
    console.log("2nd useEffect");

    async function fetchAssetMetadata(assetUnit: string) {
      try {
        const metadata = await blockfrostProvider.fetchAssetMetadata(assetUnit);
        // console.log("Asset Metadata:");
        // console.log(metadata);
        const assetName = assetUnit.replace(policyId, "");
        const assetNameString = Buffer.from(assetName, "hex").toString("utf-8");
        let imageSrc = "";
        if (metadata.files && metadata.files.length > 0) {
          imageSrc = metadata.files
            .map((file: { src: any[]; }) => file.src.join("").replace("ipfs://", "ipfs.io/ipfs/").replace("https://", ""))
            .join("");
        } else {
          imageSrc = metadata.image.join("");
        }
        const isImageSrcPresent = Array.isArray(mdata) && mdata.some((item) => item.imageSrc === `https://${imageSrc}`);
       if (!isImageSrcPresent) {
        setMetadata((prevMetadata) => [
          ...prevMetadata,
          {
            name: metadata.name,
            files: metadata.files,
            image: metadata.image,
            mediaType: metadata.mediaType,
            description: metadata.description,
            unit: assetUnit, // Store the unit from "data"
            policyId: policyId, // Store the policyId
            assetName: assetNameString, // Store the remaining part after removing the policyId from unit
            imageSrc: `https://${imageSrc}`,
          },
        ]);
      }
      } catch (error) {
        console.error("Error fetching asset metadata:", error);
      }
    }

    if (mdata.length === 0 && data.length > 0) {
      data.forEach((item) => {
        fetchAssetMetadata(item.unit);
      });
    }
  }, [data, mdata]);

  return (
    <>
      <Head>
        <title>NFTs by Rahat</title>
        <meta
          name="description"
          content="Plutus Project-Based Learning from Gimbalabs"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Divider w="70%" mx="auto" pb="10" />
      <Box w={["100%", "80%"]} mx="auto" my="10">
        <Heading style={{ display: "flex", justifyContent: "center" }}>
          PPBL 2023 NFTs
          {/* <CardanoWallet /> */}
        </Heading>
        <Box mx="auto" my="5" p="5" border="1px" borderRadius="md">
          <Heading size="md" pb="3" textAlign={"center"}>
            This page finds all the assets for policyId: {policyId} 
            <br/>and then
            fetch's the metadata for those assets It is similar to the
            information provided at
            <CLink
              color="teal.400"
              href="https://plutuspbl.io/modules/203/nft-gallery"
            >
              {" "}
              PPBL Contributors.
            </CLink>
          </Heading>
          <Text fontSize="24" pb="3" textAlign={"center"}>You can connect with me through <CLink
              color="teal.400"
              href="https://rahatsayyed.netlify.app/"
            >
              {" "}
              here.
            </CLink></Text>
          <Divider w="90%" mx="auto" mb="10" />
          {mdata ? (
            <>
           

              <div style={{ display: "flex", flexWrap: "wrap", gap: "20px", margin: "auto" }}>
                {mdata.map((Item: { imageSrc: string | undefined; assetName: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | React.ReactFragment | React.ReactPortal | null | undefined; }, index: React.Key | null | undefined) => (
                  <Box
                    key={index}
                    borderRadius="md"
                    p="4"
                    py={6}
                    w={{ base: "100%", sm: "45%", md: "32%", lg: "32%" }}
                    h="450px"
                    bg="#DDD8C4"
                    marginBottom="20px"
                    
                    >
                    <div  style={{ height: "90%", borderRadius: "3px", overflow: "hidden"}}>
                      <img src={Item.imageSrc}  style={{ maxHeight:"98%", margin: "auto"}} />
                    </div>
                      <Text  textAlign={"center"} fontSize={22} bg={"red"} borderRadius={3}>{Item.assetName}</Text>
                  </Box>
                ))}
                </div>

            </>
          ) : (
            <Box
              mx="auto"
              my="10"
              style={{ display: "flex", justifyContent: "center" }}
            >

              <Text>Loading NFTs ...</Text>
            </Box>
          )}
        </Box>
      </Box>
    </>
  );
}

// DDD8c4
