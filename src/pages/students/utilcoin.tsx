import Head from "next/head";
import { Box, Divider, Link as CLink, Heading, Text } from "@chakra-ui/react";
import { CardanoWallet, useAssets, useWallet } from "@meshsdk/react";
import { ContributorComponent } from "@/src/components/ContributorComponent";
import { QueryComponent } from "@/src/components/QueryComponent";
import { QueryContributorTokenDetails } from "@/src/components/QueryContributorTokenDetails";
import Image from 'next/image'
import Link from "next/link";
import { useEffect, useState } from "react";
import { KoiosProvider } from "@meshsdk/core";

export default function UtilCoinTemplate() {

  const { wallet, connected, name, connecting, connect, disconnect, error } = useWallet();
  const assets = useAssets();

  const koiosProvider = new KoiosProvider("preprod");
  const [data, setData] = useState([]);
  const [mdata, setMetadata] = useState("");
  const policyId = "2a384dc205a97463577fc98b704b537f680c0eba84126eb7d5857c86";

  useEffect(() => {
    async function findAssetsByPolicyId(policyId: string) {
      try {
        let cursor = 1;
        let allAssets: any[] | ((prevState: never[]) => never[]) = [];

        while (true) {
          const response = await koiosProvider.fetchCollectionAssets(
            policyId,
            cursor
          );
          const assets = response.assets;
          console.log("Assets with Policy ID:", policyId);
          // console.log(assets);

          if (Array.isArray(assets) && assets.length > 0) {
            allAssets = [...allAssets, ...assets];
          } else {
            break;
          }

          if (response.next === null) {
            break;
          }

          // Increment cursor for the next page
          cursor++;
        }
        setData(allAssets);
        console.log("useeffect", allAssets);
        // allAssets.forEach((item) => {
        //   fetchAssetMetadata(item.unit);
        // });
      } catch (error) {
        console.error("Error fetching assets:", error);
      }
    }

    // Call the function
    findAssetsByPolicyId(policyId);
    console.log("1st useEffect");
  }, []);


  useEffect(() => {
    console.log("2nd useEffect");

    async function fetchAssetMetadata(assetUnit: string) {
      try {
        const metadata = await koiosProvider.fetchAssetMetadata(assetUnit);
        // console.log("Asset Metadata:");
        // console.log(metadata);
        const assetName = assetUnit.replace(policyId, "");
        const assetNameString = Buffer.from(assetName, "hex").toString("utf-8");
        let imageSrc = "";
        if (metadata.files && metadata.files.length > 0) {
          imageSrc = metadata.files
            .map((file: { src: any[]; }) => file.src.join("").replace("ipfs://", "ipfs.io/ipfs/").replace("https://", ""))
            .join("");
        } else {
          imageSrc = metadata.image.join("");
        }
        const isImageSrcPresent = Array.isArray(mdata) && mdata.some((item) => item.imageSrc === `https://${imageSrc}`);
       if (!isImageSrcPresent) {
        setMetadata((prevMetadata) => [
          ...prevMetadata,
          {
            name: metadata.name,
            files: metadata.files,
            image: metadata.image,
            mediaType: metadata.mediaType,
            description: metadata.description,
            unit: assetUnit, // Store the unit from "data"
            policyId: policyId, // Store the policyId
            assetName: assetNameString, // Store the remaining part after removing the policyId from unit
            imageSrc: `https://${imageSrc}`,
          },
        ]);
      }
      } catch (error) {
        console.error("Error fetching asset metadata:", error);
      }
    }

    if (mdata.length === 0 && data.length > 0) {
      data.forEach((item) => {
        fetchAssetMetadata(item.unit);
      });
    }
  }, [data, mdata]);

  return (
    <>
      <div className="container">
      <Head>
        <title>PPBL 2023 Playground</title>
        <meta name="description" content="Plutus Project-Based Learning from Gimbalabs" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="https://raw.githubusercontent.com/utilcoin/utilcoin/main/docs/favicon.ico" />
        <link
          href="https://meshjs.dev/css/template.css"
          rel="stylesheet"
          key="mesh-demo"
        />
      </Head>

      <div className="relative overflow-hidden bg-black">
      <Divider w="70%" mx="auto" pb="10" />
      <Box w={["100%", "70%"]} mx="auto" my="10">
        <Heading>PPBL 2023 template example by Gimbalabs</Heading>
        <Box mx="auto" my="5" p="5" border="1px" borderRadius="md">
          <Heading size="md" pb="3">
            Checking your PPBL 2023 Contributor Token:
          </Heading>
          <div style={{ position: 'absolute', top: 0, right: 0 }}>
            <CardanoWallet />
          </div>
          <ContributorComponent />
          <QueryComponent />
          <QueryContributorTokenDetails />
        </Box>
      </Box>

      <Box w={["100%", "70%"]} mx="auto" my="10">
        <Heading>
          PPBL 2023 NFTs example by Rahat

        </Heading>
        <Box mx="auto" my="5" p="5" border="1px" borderRadius="md">
          <Heading size="md" pb="3" textAlign={"center"}>
            This page finds all the assets for policyId: {policyId}
            <br/>and then
            fetch's the metadata for those assets It is similar to the
            information provided at
            <CLink
              color="teal.400"
              href="https://plutuspbl.io/modules/203/nft-gallery"
            >
              {" "}
              PPBL Contributors.
            </CLink>
          </Heading>

          <Divider w="90%" mx="auto" mb="10" />
          {mdata ? (
            <>


              <div style={{ display: "flex", flexWrap: "wrap", gap: "20px", margin: "auto" }}>
                {mdata.map((Item: { imageSrc: string | undefined; assetName: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | React.ReactFragment | React.ReactPortal | null | undefined; }, index: React.Key | null | undefined) => (
                  <Box
                    key={index}
                    borderRadius="md"
                    p="4"
                    py={6}
                    w={{ base: "100%", sm: "45%", md: "32%", lg: "32%" }}
                    h="450px"
                    bg="#DDD8C4"
                    marginBottom="20px"

                    >
                    <div  style={{ height: "90%", borderRadius: "3px", overflow: "hidden"}}>
                      <img src={Item.imageSrc}  style={{ maxHeight:"98%", margin: "auto"}} />
                    </div>
                      <Text  textAlign={"center"} fontSize={22} bg={"red"} borderRadius={3}>{Item.assetName}</Text>
                  </Box>
                ))}
                </div>

            </>
          ) : (
            <Box
              mx="auto"
              my="10"
              style={{ display: "flex", justifyContent: "center" }}
            >

              <Text>Loading NFTs ...</Text>
            </Box>
          )}
        </Box>
      </Box>

      <Box w={["100%", "70%"]} mx="auto" my="10">

        <Heading>Check all my Tokens by nelsonksh</Heading>

        <Box mx="auto" my="5" p="5" border="1px" borderRadius="md">

          <Box display="flex" flexDirection="column" justifyContent="center" alignItems="center">
            <Text mb="4">Please connect your wallet to see what tokens your wallet holds</Text>
            <CardanoWallet />
          </Box>

          <Box>
            {assets?.map((item, i) => (
              <Box mt="3" key={i}>
                {item.quantity} {item.assetName}
              </Box>
            )
            )}
          </Box>

        </Box>

      </Box>

      </div>
      <footer className="footer">
        <div className="relative flex place-items-center before:absolute before:h-[300px] before:w-[480px] before:-translate-x-1/2 before:rounded-full before:bg-gradient-radial before:from-white before:to-transparent before:blur-2xl before:content-[''] after:absolute after:-z-20 after:h-[180px] after:w-[240px] after:translate-x-1/3 after:bg-gradient-conic after:from-sky-200 after:via-blue-200 after:blur-2xl after:content-[''] before:dark:bg-gradient-to-br before:dark:from-transparent before:dark:to-blue-700/10 after:dark:from-sky-900 after:dark:via-[#0141ff]/40 before:lg:h-[360px]">
          <Link href="https://github.com/utilcoin">
            <Image
              className="relative  opacity-50"
              src="https://raw.githubusercontent.com/utilcoin/utilcoin/ecf83b17314bf10c9e8433d3a2d8984702a7b0fb/docs/static/images/logo-blue-transparent-2.svg"
              alt="UtilCoin Logo"
              width={250}
              height={250}
              priority
            /></Link>
        </div>

      </footer>
      </div>
    </>
  );
}
